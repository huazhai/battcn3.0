package com.battcn.management.webmagic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author Levin
 * @since 2018/03/09
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookLabel {

    private String link;
    private String title;
    private String content;
}

