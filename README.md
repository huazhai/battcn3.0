## 开始维护

[http://demo.battcn.com](http://demo.battcn.com "演示地址")

# 开发内容

目前会编写一个 书城/影城  涵盖 `爬虫、代理、管理系统、阅读、下载` 本项目会分为 `dubbo` 与 `springcloud` 版本，初期以 `dubbo`为主


鉴于 `dubbo` 已经开始维护了 , 本项目也即将开启维护之旅

由于 `dubbo-spring-boot-starter（现在还是开发版）`，所以暂未升级 `spring-boot2.x`,依旧采用的`1.5.10.RELEASE` 版本



## 技术栈

- **spring-boot**
- **dubbo**
- **mybatis**
- **shiro**
- **zookeeper**
- **swagger**

----------

**后续引入 redis、rabbitmq 等技术栈**



